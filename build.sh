#!/bin/bash

mkdir -p ./build
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./build/nsq-message-checker -v .
