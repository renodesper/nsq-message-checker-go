package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/tealeg/xlsx"
)

var file *xlsx.File
var sheet *xlsx.Sheet
var row *xlsx.Row
var cell *xlsx.Cell
var client = &http.Client{
	Timeout: time.Second * 10,
}
var nsqStatURL = "http://localhost:4151/stats"
var slackURL = "https://hooks.slack.com/services/<SLACK-TOKEN>"
var googleDocURL = "https://docs.google.com/spreadsheets/d/<SOME-URL>"
var filePath = "<FOLDER-PATH>/NSQ Statistic Report.xlsx"

func main() {
	var err error

	uris := []string{
		"go_optimization_rule",
		"go_optimization_rule_time_based_action",
		"go_sync_insight",
		"go_sync_insight_saver",
		"go_sync_insight_breakdown",
		"go_sync_insight_breakdown_saver",
	}

	file = xlsx.NewFile()
	sheet, err = file.AddSheet("Sheet1")
	if err != nil {
		panic(fmt.Errorf("Error when adding sheet: %s", err.Error()))
	}

	createHeader()

	for _, uri := range uris {
		checkNsqStat(uri, "job")
	}

	err = file.Save(filePath)
	if err != nil {
		panic(err)
	}

	message := fmt.Sprintf("NSQ Statistic Report has been compiled, please update the report on Google Doc:\n```%s```", googleDocURL)
	sendMessageToSlack(message)
}

func createHeader() {
	row = sheet.AddRow()
	cell = row.AddCell()
	cell.Value = "Topic"
	cell = row.AddCell()
	cell.Value = "Date"
	cell = row.AddCell()
	cell.Value = "Time"
	cell = row.AddCell()
	cell.Value = "Message"
	cell = row.AddCell()
	cell.Value = "Total Message"
	cell = row.AddCell()
	cell.Value = "Connection"
}

func checkNsqStat(topic string, channel string) {
	var url = fmt.Sprintf("%s?format=json&topic=%s&channel=%s", nsqStatURL, topic, channel)

	resp, err := client.Get(url)
	if err != nil {
		panic(fmt.Errorf("Error when getting stat: %s", err.Error()))
	}
	defer resp.Body.Close()

	var stat map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&stat)
	if err != nil {
		panic(fmt.Errorf("Error when decoding http.Response: %s", err.Error()))
	}

	topics := stat["topics"].([]interface{})

	var depth int
	var messageCount int
	var connections int

	for _, tMap := range topics {
		var t = tMap.(map[string]interface{})

		if t["topic_name"].(string) == topic {
			var cSlice = t["channels"].([]interface{})

			for _, cMap := range cSlice {
				var c = cMap.(map[string]interface{})

				if c["channel_name"].(string) == channel {
					messageCount = int(interfaceToFloat64(t, "message_count"))
					depth = int(interfaceToFloat64(c, "depth"))
					connections += len(c["clients"].([]interface{}))

					break
				}
			}

			break
		}
	}

	var date = time.Now().Format("02/01/2006")
	var time = time.Now().Format("15:04")

	fmt.Printf("Url: %s\n", url)
	fmt.Printf("Topic: %s\n", topic)
	fmt.Printf("Date: %s\n", date)
	fmt.Printf("Time: %s\n", time)
	fmt.Printf("Messages: %d\n", depth)
	fmt.Printf("Total Message: %d\n", messageCount)
	fmt.Printf("Connection: %d\n\n", connections)

	row = sheet.AddRow()
	cell = row.AddCell()
	cell.Value = topic
	cell = row.AddCell()
	cell.Value = date
	cell = row.AddCell()
	cell.Value = time
	cell = row.AddCell()
	cell.Value = strconv.Itoa(depth)
	cell = row.AddCell()
	cell.Value = strconv.Itoa(messageCount)
	cell = row.AddCell()
	cell.Value = strconv.Itoa(connections)

	// sendStatisticToSlack(topic, depth, messageCount, connections)
}

func sendStatisticToSlack(topic string, depth int, messageCount int, connections int) {
	text := fmt.Sprintf(
		"*Topic: %s*\n```Date:          %s\nTime:          %s\nMessages:      %d\nTotal Message: %d\nConnection:    %d```",
		topic, time.Now().Format("02/01/2006"), time.Now().Format("15:04"), depth, messageCount, connections)

	message := map[string]interface{}{
		"name":   "NSQ Statistic Checker",
		"text":   text,
		"mrkdwn": true,
	}
	body, _ := json.Marshal(message)

	resp, err := client.Post(slackURL, "application/json", bytes.NewBuffer(body))
	if err != nil {
		panic(fmt.Errorf("Error when sending message: %s", err.Error()))
	}
	defer resp.Body.Close()

	response, _ := ioutil.ReadAll(resp.Body)
	fmt.Printf("Message was sent to slack: %s\n", string(response))
}

func sendMessageToSlack(text string) {
	message := map[string]interface{}{
		"name": "NSQ Statistic Checker",
		"text": text,
	}
	body, _ := json.Marshal(message)

	resp, err := client.Post(slackURL, "application/json", bytes.NewBuffer(body))
	if err != nil {
		panic(fmt.Errorf("Error when sending message: %s", err.Error()))
	}
	defer resp.Body.Close()

	response, _ := ioutil.ReadAll(resp.Body)
	fmt.Printf("Message was sent to slack: %s\n", string(response))
}

func interfaceToFloat64(m map[string]interface{}, key string) (f float64) {
	switch v := m[key].(type) {
	case int:
		f = float64(v)
	default:
		f = v.(float64)
	}

	return f
}
